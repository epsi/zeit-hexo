# Hexo Bulma Test Drive

An example of Hexo site using Bulma for personal learning purpose.

This is basically a Bulma version of Hugo Tutorial.

> Jekyll (Liquid) + Bulma (Custom CSS)

![Hexo Bulma: Tutor][hexo-bulma-preview]

Please refer to original tutorial:

* [Hexo Step by Step Repository][tutorial-hexo]

-- -- --

What do you think ?

[tutorial-hexo]:        https://gitlab.com/epsi-rns/tutor-hexo-bulma/
[hexo-bulma-preview]:   https://gitlab.com/epsi-rns/tutor-hexo-bulma/raw/master/preview/hexo-bulma-preview.png
